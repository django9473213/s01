# Generated by Django 4.1.7 on 2023-04-02 09:35

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('todolist', '0007_remove_user_user'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='user',
            field=models.ForeignKey(default='', on_delete=django.db.models.deletion.RESTRICT, to=settings.AUTH_USER_MODEL),
        ),
    ]
