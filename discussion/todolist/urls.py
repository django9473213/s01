from django.urls import path
# import the views.py from the same folder
from . import views


app_name = 'todolist'

urlpatterns = [
	path('index', views.index, name = 'index'),
	path('todoitem/<int:todoitem_id>', views.todoitem, name = "viewtodoitem"),
	path('event/<int:event_id>', views.event, name = "viewevent"),
	path('register', views.register, name = "register"),
	path('change_password', views.change_password, name = "change_password"),
	path('login', views.login_user, name = "login"),
	path('logout', views.logout_user, name = "logout"),
	path('add_task', views.add_task, name = "add_task"),
	path('<int:todoitem_id>/update_task', views.update_task, name="update_task"),
	path('<int:todoitem_id>/delete', views.delete_task, name = "delete_task"),
	path('add_event',views.add_event, name = "add_event"),
	path('user', views.user, name = "user"),
	path('user_profile/<int:user_id>', views.user_profile, name = "viewuser_profile"),
	path('<int:user_id>/update_profile', views.update_profile, name = "update_profile")
]


