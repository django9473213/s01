from django.contrib import admin

# Register your models here.


from .models import ToDoItem

admin.site.register(ToDoItem)

from .models import Event

admin.site.register(Event)

from .models import User

admin.site.register(User)